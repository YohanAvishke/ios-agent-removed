//
// Created by Yohan Avishke Ediriweera on 4/23/20.
// Copyright (c) 2020 WSO2. All rights reserved.
//

#import "WebSocketSessionHandler.h"
#import <SocketRocket/SocketRocket.h>

/*!
 * Websocket delegate
 */
@interface WebSocketClient : SRWebSocket <SRWebSocketDelegate>

- (id)init:(NSString *)url;

@end
