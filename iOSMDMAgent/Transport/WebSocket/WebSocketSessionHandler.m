//
// Created by Yohan Avishke Ediriweera on 4/23/20.
// Copyright (c) 2020 WSO2. All rights reserved.
//

#import "WebSocketSessionHandler.h"

@implementation WebSocketSessionHandler {
    WebSocketClient *_webSocketClient;
    NSInteger *_operationId;
    NSObject *const writeLockObject;
}

+ (WebSocketSessionHandler *)getInstance {
    static WebSocketSessionHandler *webSocketSession = nil;
    static dispatch_once_t onceToken; // onceToken = 0
    dispatch_once(&onceToken, ^{
        webSocketSession = [[WebSocketSessionHandler alloc] init];
    });

    return webSocketSession;
}

- (void)initializeSession:(NSString *)serverUrl :(NSInteger *)operationId :(NSString *)websocketToken {
    if (_operationId == operationId) {
        NSLog(@"Error, failed to initialize Websocket connection. Operation : %li is already connected",
                (long) operationId);
        return;
    }
    //TODO read server ip from app config
    if ([serverUrl containsString:@"localhost"]) {
        serverUrl = [serverUrl stringByReplacingOccurrencesOfString:@"localhost" withString:@"192.168.8.100"];
    }
    _operationId = operationId;
    if (_webSocketClient && (_webSocketClient.readyState == 0 || _webSocketClient.readyState == 1)) {
        [[WebSocketSessionHandler getInstance] endSession];
        NSLog(@"Warning, existing Websocket connection was closed to create a new one for operation : %li",
                (long) operationId);
    }

    NSString *url = [NSString stringWithFormat:@"%@%@/%@/%li?websocketToken=%@",
                                               serverUrl, [URLUtils readEndpoints][REMOTE_SESSION_PATH],
                                               [MDMUtils getDeviceUDID], (long) operationId, websocketToken];
    _webSocketClient = [[WebSocketClient alloc] init:url];
    _webSocketClient.delegate = _webSocketClient;
    [_webSocketClient open];
}

- (void)handleSessionMessage:(NSString *)message {
    id jsonMessage = [NSJSONSerialization JSONObjectWithData:[message dataUsingEncoding:NSUTF8StringEncoding]
                                                     options:NSJSONReadingMutableContainers
                                                       error:nil];
    NSString *operationCode = [jsonMessage objectForKey:@"code"];

    if (operationCode != nil) {
        if ([operationCode containsString:REMOTE_SCREEN]) {
            NSString *payload = [jsonMessage objectForKey:@"payload"];

            if (payload != nil) {
                id jsonPayload = [NSJSONSerialization JSONObjectWithData:[payload dataUsingEncoding:NSUTF8StringEncoding]
                                                                 options:NSJSONReadingMutableContainers
                                                                   error:nil];
                NSString *action = [jsonPayload objectForKey:@"action"];

                if ([action isEqualToString:@"start"]) {
                    if (@available(iOS 12.0, *)) {
                        [MDMUtils setRemoteScreenStatus:REMOTE_SCREEN_STARTED];
                    } else {
                        NSLog(@"Error, Operation code: %@ is not supported for versions below 12.0", REMOTE_SCREEN);
                    }
                } else if ([action isEqualToString:@"stop"]) {
                    [MDMUtils setRemoteScreenStatus:REMOTE_SCREEN_STOPPED];
                }
            } else {
                NSLog(@"Error, Message payload does not contain screen capture inputs");
            }
        } else {
            NSLog(@"Error, Operation code is not supported");
        }
    }
}

- (void)sendByteMessage:(NSData *)message {
    if (_webSocketClient && _webSocketClient.readyState == 1) {
        @synchronized (writeLockObject) {
            [_webSocketClient send:message];
        }
    } else if (message == nil) {
        NSLog(@"Warning, Message cannot be null for operation id %li", (long) _operationId);
    } else {
        NSLog(@"Error, Websocket client already closed for operation id %li", (long) _operationId);
    }

}

- (void)sendStringMessage:(NSString *)message {
    NSLog(@"%li", (long) _webSocketClient.readyState);
    if (_webSocketClient && _webSocketClient.readyState == 1) {
        @synchronized (writeLockObject) {
            [_webSocketClient send:message];
        }
    } else if (message == nil) {
        NSLog(@"Warning, Message cannot be null for operation id %li", (long) _operationId);
    } else {
        NSLog(@"Error, Websocket client not successfully connected for operation id %li. Client state: %d",
                (long) _operationId, (int) _webSocketClient.readyState);
    }
}

- (void)endSession {
    if (_webSocketClient && (_webSocketClient.readyState == 0 || _webSocketClient.readyState == 1)) {
        [_webSocketClient close];
    }
    _operationId = nil;
    _webSocketClient = nil;
}

@end