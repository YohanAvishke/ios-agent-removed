//
// Created by Yohan Avishke Ediriweera on 4/23/20.
// Copyright (c) 2020 WSO2. All rights reserved.
//

#import "WebSocketClient.h"

@implementation WebSocketClient

- (id)init:(NSString *)url {
    self = [super initWithURL:[NSURL URLWithString:url]];
    return self;
}

- (void)webSocketDidOpen:(SRWebSocket *)webSocket {
    NSLog(@"WebSocket connected successfully.");
}

- (void)webSocket:(SRWebSocket *)webSocket didReceiveMessage:(id)message {
    [[WebSocketSessionHandler getInstance] handleSessionMessage:message];
    NSLog(@"Message: %@", message);
}

- (void)webSocket:(SRWebSocket *)webSocket didFailWithError:(NSError *)error {
    [[WebSocketSessionHandler getInstance] endSession];
    NSLog(@"Error, Websocket Failed With Error %@", error);
}

- (void)webSocket:(SRWebSocket *)webSocket didCloseWithCode:(NSInteger)code reason:(NSString *)reason
         wasClean:(BOOL)wasClean {
    [[WebSocketSessionHandler getInstance] endSession];
    NSLog(@"WebSocket closed with reason: %@", reason);
}

@end
