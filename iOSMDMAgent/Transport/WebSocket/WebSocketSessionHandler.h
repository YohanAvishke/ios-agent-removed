//
// Created by Yohan Avishke Ediriweera on 4/23/20.
// Copyright (c) 2020 WSO2. All rights reserved.
//

#import "MDMConstants.h"
#import "MDMUtils.h"
#import "URLUtils.h"
#import "WebSocketClient.h"

/*!
 * Class for handling web socket sessions
 */
@interface WebSocketSessionHandler : NSObject

/*!
 * Get a singleton Instance of WebSocketSessionHandler
 * @return WebSocketSessionHandler instance
*/
+ (WebSocketSessionHandler *)getInstance;

/*!
 * Create a Websocket session instance
 * @param serverURL url of the Websocket endpoint
 * @param operationId operation id
 * @param websocketToken unique token to identify Websocket session
 */
- (void)initializeSession:(NSString *)serverURL :(NSInteger *)operationId :(NSString *)websocketToken;

/*!
 * Handle messages incoming from server
 * @param message message data
 */
- (void)handleSessionMessage:(NSString *)message;

/*!
 * Send a Byte message from the client to the server
 * @param message byte message
 */
- (void)sendByteMessage:(NSData *)message;

/*!
 * Send a String message from the client to the server
 * @param message string message
 */
- (void)sendStringMessage:(NSString *)message;

/*!
 * End the Websocket client session
 */
- (void)endSession;

@end
