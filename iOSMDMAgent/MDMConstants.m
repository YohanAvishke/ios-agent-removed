//
//  MDMConstants.m
//  iOSMDMAgent
//


#import "MDMConstants.h"

@implementation MDMConstants

NSString *const OK_BUTTON_TEXT = @"OK";
NSString *const CANCEL_BUTTON_TEXT = @"Cancel";
NSString *const EMPTY_URL = @"Error";
NSString *const EMPTY_URL_MESSAGE = @"Server url is empty. Please type a valid server url.";
NSString *const AUTH_ERROR_TITLE = @"Authentication Error";
NSString *const AUTH_ERROR_MESSAGE = @"Authentication Error. Please check your credentials.";
NSString *const INVALID_SERVER_URL = @"Invalid Server URL";
NSString *const INVALID_SERVER_URL_MESSAGE = @"Invalid server url. Please check your server url.";
NSString *const UNREGISTER_ERROR = @"Error";
NSString *const UNREGISTER_ERROR_MESSAGE = @"Error occurred while unregistering. Please try again.";

float const HTTP_REQUEST_TIME = 60.0f;
int HTTP_OK = 200;
int HTTP_CREATED = 201;
int OAUTH_FAIL_CODE = 401;
int HTTP_BAD_REQUEST = 400;
NSString *const ENDPOINT_FILE_NAME = @"Endpoints";
NSString *const EXTENSION = @"plist";
NSString *const TOKEN_PUBLISH_URI = @"TOKEN_PUBLISH_URI";
NSString *const ENROLLMENT_URI = @"ENROLLMENT_URI";
NSString *const OPERATION_URI = @"OPERATION_URI";
NSString *const LOCATION_PUBLISH_URI = @"LOCATION_PUBLISH_URI";
NSString *const SERVER_URL = @"SERVER_URL";
NSString *const CONTEXT_URI = @"SERVER_CONTEXT";
NSString *const API_PORT = @"API_PORT";
NSString *const ENROLMENT_PORT = @"ENROLMENT_PORT";
NSString *const TOKEN = @"token";
NSString *const GET = @"GET";
NSString *const POST = @"POST";
NSString *const PUT = @"PUT";
NSString *const ACCEPT = @"Accept";
NSString *const CONTENT_TYPE = @"Content-Type";
NSString *const APPLICATION_JSON = @"application/json";
NSString *const LATITIUDE = @"latitude";
NSString *const LONGITUDE = @"longitude";
NSString *const UNENROLLMENT_PATH = @"UNENROLLMENT_PATH";
NSString *const AUTHORIZATION_BASIC = @" Basic ";
NSString *const AUTHORIZATION_BEARER = @" Bearer ";
NSString *const AUTHORIZATION = @"Authorization";
NSString *const REFRESH_TOKEN_URI =@"REFRESH_TOKEN_URI";
NSString *const REFRESH_TOKEN_LABEL = @"refresh_token";
NSString *const GRANT_TYPE = @"grant_type";
NSString *const GRANT_TYPE_VALUE = @"refresh_token";
NSString *const FORM_ENCODED = @"application/x-www-form-urlencoded";
NSString *const OPERATION_ID_RESPOSNE = @"operationId";
NSString *const STATUS = @"status";
NSString *const ENROLLMENT_URL = @"ENROLLMENT_URL";
NSString *const EFFECTIVE_POLICY_PATH = @"EFFECTIVE_POLICY_PATH";
NSString *const TOKEN_REFRESH_URI = @"TOKEN_REFRESH_URI";
NSString *const TENANT_NAME = @"tenantDomain";
NSString *const USERNAME = @"username";
NSString *const PASSWORD = @"password";
NSString *const AGENT_BASED_ENROLLMENT = @"AGENT_BASED_ENROLLMENT";
NSString *const CA_DOWNLOAD_PATH = @"CA_DOWNLOAD_PATH";
NSString *const AUTH_PATH = @"AUTH_PATH";
NSString *const LICENSE_PATH = @"LICENSE_PATH";
NSString *const ENROLL_PATH = @"ENROLL_PATH";
NSString *const IS_ENROLLED_PATH = @"IS_ENROLLED_PATH";
NSString *const MOBICONFIG_PATH = @"MOBICONFIG_PATH";
NSString *const AUTO_ENROLLMENT = @"AUTO_ENROLLMENT";
NSString *const AUTO_ENROLLMENT_STATUS_PATH = @"AUTO_ENROLLMENT_STATUS_PATH";
NSString *const AUTO_ENROLLMENT_COMPLETED = @"AUTO_ENROLLMENT_COMPLETED";
NSString *const IS_AGENT_AVAILABLE = @"isAgentAvailable";

int LOCATION_DISTANCE_FILTER = 100;
NSString *const UDID = @"DEVICE_UDID";
NSString *const APS = @"aps";
NSString *const EXTRA = @"extra";
NSString *const OPERATION = @"operation";
NSString *const OPERATION_ID = @"operationId";
NSString *const SOUND_FILE_NAME = @"sound";
NSString *const SOUND_FILE_EXTENSION = @"caf";
NSString *const ENCLOSING_TAGS = @"<>";
NSString *const TOKEN_KEYCHAIN = @"TOKEN_KEYCHAIN1";
NSString *const CLIENT_DETAILS_KEYCHAIN = @"CLIENT_DETAILS_KEYCHAIN";
NSString *const ENROLL_STATUS = @"ENROLL_STATUS";
NSString *const ENROLLED = @"enrolled";
NSString *const UNENROLLED = @"unenrolled";
NSString *const LOCATION_OPERATION_ID = @"LOCATION_OPERATION_ID";
NSString *const ACCESS_TOKEN = @"ACCESS_TOKEN";
NSString *const REFRESH_TOKEN = @"REFRESH_TOKEN";
NSString *const CLIENT_CREDENTIALS = @"CLIENT_CREDENTIALS";
NSString *const TENANT_DOMAIN = @"TENANT_DOMAIN";
NSString *const LOCATION_UPDATED_TIME = @"LOCATION_UPDATED_TIME";
NSString *const LICENSE_TEXT = @"LICENSE_TEXT";
NSString *const CHALLANGE_TOKEN = @"CHALLANGE_TOKEN";
NSString *const AUTHENTICATION_FAIL = @"AUTHENTICATION_FAIL";
NSString *const EMPTY_USERNAME = @"EMPTY_USERNAME";
NSString *const EMPTY_PASSWORD = @"EMPTY_PASSWORD";
NSString *const ENROLLMENT_SUCCESS = @"ENROLLMENT_SUCCESS";

// Extensions
NSString *const BROADCAST_EXTENSION = @"BroadcastService.appex";

// Operation status
NSString *const OPERATION_VALUE_PENDING = @"PENDING";
NSString *const OPERATION_VALUE_PROGRESS = @"IN_PROGRESS";
NSString *const OPERATION_VALUE_COMPLETED = @"COMPLETED";
NSString *const OPERATION_VALUE_ERROR = @"ERROR";

NSString *const REMOTE_SESSION_PATH = @"REMOTE_SESSION_PATH";

// Screen share
NSString *const REMOTE_SCREEN = @"REMOTE_SCREEN";
NSString *const REMOTE_SCREEN_STATUS = @"REMOTE_SCREEN_STATUS";
NSString *const REMOTE_SCREEN_STARTED = @"REMOTE_SCREEN_STARTED";
NSString *const REMOTE_SCREEN_STOPPED = @"REMOTE_SCREEN_STOPPED";

@end
