//
//  ViewController.h
//  iOSMDMAgent
//
//  Created by Dilshan Edirisuriya on 2/5/15.
//  Copyright (c) 2015 WSO2. All rights reserved.
//

#import "SDKProtocol.h"
#import "MDMConstants.h"
#import "WebSocketSessionHandler.h"

#import <UIKit/UIKit.h>
#import <ReplayKit/ReplayKit.h>

@interface ViewController : UIViewController <SDKProtocol, UIAlertViewDelegate>

@property (weak, nonatomic) IBOutlet UIButton *btnUnRegister;
@property (strong, nonatomic) IBOutlet UIButton *btnRefresh;
@property (strong, nonatomic) IBOutlet UILabel *lbLocationSync;
@property (strong, nonatomic) IBOutlet UIButton *startBroadcast;
@property (strong, nonatomic) IBOutlet UIButton *stopBroadcast;

- (IBAction)clickOnUnRegister:(id)sender;

- (IBAction)refresh:(id)sender;

- (void)addBroadcastPickerView API_AVAILABLE(ios(12.0));

- (void)removeBroadcastPickerView API_AVAILABLE(ios(12.0));

- (IBAction)clickStartBroadcast:(id)sender;

- (IBAction)clickStopBroadcast:(id)sender;

@end

