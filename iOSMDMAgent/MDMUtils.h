//
//  MDMUtils.h
//  iOSMDMAgent
//

#import <Foundation/Foundation.h>
#import "MDMConstants.h"

@interface MDMUtils : NSObject



+ (void)saveDeviceUDID:(NSString *)udid;
+ (NSString *)getDeviceUDID;
+ (NSString *) getEnrollStatus;
+ (void) setEnrollStatus: (NSString *)value;
+ (NSString *) getLocationOperationId;
+ (void) setLocationOperationId: (NSString *)value;
+ (void)savePreferance:(NSString *)key value:(NSString *)val;
+ (NSString *)getPreferance:(NSString *)key;
+ (void)setAccessToken:(NSString *)accessToken;
+ (void)setRefreshToken:(NSString *)refreshToken;
+ (NSString *)getAccessToken;
+ (NSString *)getRefreshToken;
+ (void)setClientCredentials:(NSString *)clientCredentials;
+ (NSString *)getClientCredentials;
+ (void)setTenantDomain:(NSString *)tenantDomain;
+ (NSString *)getTenantDomain;
+ (void)setLocationUpdatedTime;
+ (NSString *)getLocationUpdatedTime;
+ (NSString *) getRemoteScreenStatus;
+ (void) setRemoteScreenStatus:(NSString *)status;
+ (void)clearPrefs;

@end
