//
//  SampleHandler.m
//  BroadcastService
//
//  Created by Yohan Avishke Ediriweera on 4/29/20.
//  Copyright © 2020 WSO2. All rights reserved.
//

#import "SampleHandler.h"

@implementation SampleHandler

- (void)broadcastStartedWithSetupInfo:(NSDictionary<NSString *, NSObject *> *)setupInfo {
    NSLog(@"Broadcast started");
    // User has requested to start the broadcast. Setup info from the UI extension can be supplied but optional. 
}

- (void)broadcastPaused {
    // User has requested to pause the broadcast. Samples will stop being delivered.
}

- (void)broadcastResumed {
    // User has requested to resume the broadcast. Samples delivery will resume.
}

- (void)broadcastFinished {
    // User has requested to finish the broadcast.
}

- (void)processSampleBuffer:(CMSampleBufferRef)sampleBuffer withType:(RPSampleBufferType)sampleBufferType {
    CVImageBufferRef imageBuffer = CMSampleBufferGetImageBuffer(sampleBuffer);
    CGImageRef image = NULL;
    OSStatus createdImage = VTCreateCGImageFromCVPixelBuffer(imageBuffer, NULL, &image);

    switch (sampleBufferType) {
        case RPSampleBufferTypeVideo:
            if (createdImage == noErr) {
                UIImage *uiImage = [UIImage imageWithCGImage:image];
                [[WebSocketSessionHandler getInstance] sendByteMessage:UIImagePNGRepresentation(uiImage)];
            }
            // Handle video sample buffer for app video
            break;
        case RPSampleBufferTypeAudioApp:
            // Handle audio sample buffer for app audio
            break;
        case RPSampleBufferTypeAudioMic:
            // Handle audio sample buffer for mic audio
            break;
        default:
            break;
    }
}

@end
