//
//  SampleHandler.h
//  BroadcastService
//
//  Created by Yohan Avishke Ediriweera on 4/29/20.
//  Copyright © 2020 WSO2. All rights reserved.
//

#import "WebSocketSessionHandler.h"

#import <ReplayKit/ReplayKit.h>
#import <CoreMedia/CoreMedia.h>
#import <UIKit/UIKit.h>
#import <VideoToolbox/VideoToolbox.h>

@interface SampleHandler : RPBroadcastSampleHandler

@end
